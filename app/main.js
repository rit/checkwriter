'use strict';

var angular = require('angular');
var numeralize = require('lib/numeralize');

var app = angular.module('checkwriter',
  [
    require('angular-cookies'),
    require('angular-animate'),
  ]
);

app.directive('numeralizer', function() {
  return {
    restrict: 'E',
    scope: {
      amount: '='
    },
    controller: function($scope) {
      this.numeralize = numeralize;
    },
    controllerAs: "ctrl",
    template: '<div> {{ ctrl.numeralize(amount) }} </div>'
  }
});
