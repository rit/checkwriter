'use strict';

var ngctx = require('lib/ngctx');

describe("numeralizer directive", function() {
  var ctx;

  beforeAll(function(done) {
    ngctx(function(_ctx) {
      ctx = _ctx;
      done();
    });
  });

  it("numerizes a number", function() {
    var injector = ctx.bootstrap(['checkwriter']);
    var $rootScope = injector.get('$rootScope');
    var $compile = injector.get('$compile');
    var scope = $rootScope.$new();
    scope.tax = 22;

    var elem = $compile(
      '<numeralizer amount="tax"></numeralizer>')(scope);
    scope.$digest();

    expect(elem.html()).toContain('twenty-two');
  });
});
