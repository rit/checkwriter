function foo () {
  for (var i = 0; i < 10; i++) {
    if (i > 5) {
      return i;
    }
  };
}

var res = foo();
console.log(res);
