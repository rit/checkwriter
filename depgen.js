'use strict';

var fs = require("fs");
var acorn = require("acorn");
var walker = require("acorn/dist/walk");
var os = require("os");

var src = fs.readFileSync('./app/main.js').toString();
var ast = acorn.parse(src);


function findReq (node) {
  var self = this;
  if (node.callee.name === 'require') {
    node.arguments.forEach(function(req) {
      self.matched.push(req.value);
    });
  }
}

walker.simple(ast, {
  CallExpression: function(node) {
    if (node.callee.type === 'MemberExpression') {
      var callee = node.callee;
      if (callee.object.name === 'angular' && callee.property.name === 'module') {
        var reqcalls = node.arguments[1];
        var found = [];
        walker.simple(node, {
          CallExpression: findReq.bind({matched: found})
        });
        found.unshift('angular');
        found.push('angular-mocks');
        var body = found.join('\n') + os.EOL;
        fs.writeFileSync("modules.txt", body);
      }
    }
  }
});
