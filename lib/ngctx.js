var fs = require("fs");
var sprintf = require("underscore.string").sprintf;


function getContext (cb) {
  var entry = "tmp/main.js";
  if (!fs.existsSync(entry)) {
    throw sprintf("File %s is not found", entry);
  }

  var ngctx = require('angularcontext');
  var ctx = ngctx.Context();
  ctx.runFile(entry, function(res, err) {
    if (err) {
      cb(null);
    } else {
      cb(ctx);
    }
  });
}

module.exports = getContext;
