var numeralize = require('lib/numeralize');

describe("numeralize", function() {
  describe("numerals", function() {
    it("supports numbers 1-19", function() {
      expect(numeralize(1)).toEqual("one");
      expect(numeralize(2)).toEqual("two");
      expect(numeralize(10)).toEqual("ten");
      expect(numeralize(12)).toEqual("twelve");
      expect(numeralize(19)).toEqual("nineteen");
    });

    it("supports numbers with multiple of 10s less than 100", function() {
      expect(numeralize(10)).toEqual("ten");
      expect(numeralize(20)).toEqual("twenty");
    });

    it("supports mumbers between 21-99", function() {
      expect(numeralize(21)).toEqual("twenty-one");
      expect(numeralize(35)).toEqual("thirty-five");
      expect(numeralize(99)).toEqual("ninety-nine");
    });

    it("supports number between 100 and 999", function() {
      expect(numeralize(100)).toEqual("one hundred");
      expect(numeralize(230)).toEqual("two hundred thirty");
      expect(numeralize(999)).toEqual("nine hundred ninety-nine");
    });

    it("supports number between 1000 and 999,999", function() {
      expect(numeralize(1000)).toEqual("one thousand");
      expect(numeralize(2345)).toEqual("two thousand three hundred forty-five");
      expect(numeralize(999222)).toEqual("nine hundred ninety-nine thousand two hundred twenty-two");
    });

    it("supports number between 1000000 and 999,999,999", function() {
      expect(numeralize(1000000)).toEqual("one million");
      expect(numeralize(1002345)).toEqual("one million two thousand three hundred forty-five");
    });

  });
});
