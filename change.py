import hashlib
import os


def hash(fname):
    m = hashlib.md5()
    with open(fname) as f:
        [m.update(line) for line in f]
        return m.hexdigest()


def sumname(fname):
    if fname.startswith('tmp'):
        return '%s.sum' % fname
    return 'tmp/%s.sum' % fname


def changed(fname, hash):
    with open(sumname(fname)) as f:
        line = f.readline()
        if hash == line.strip():
            return False
        else:
            return True

    
class change(object):

    def __init__(self, fname):
        self.fname = fname

    def __enter__(self):
        self.hash = hash(self.fname)
        if not os.path.exists(sumname(self.fname)):
            self.changed = True
        else:
            self.changed =  changed(self.fname, self.hash)
        return self.changed

    def __exit__(self, exc_type, exc_value, tb):
        if self.changed:
            with open(sumname(self.fname), 'w') as f:
                f.write(self.hash)

        
if __name__ == '__main__':
    with change('modules.txt') as changed:
        if changed:
            print 'in changed'
        else:
            print 'nothing'
