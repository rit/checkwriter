(function() {
var root = this;

root.numeralize = (function(num) {
  var words = {
    1: "one",
    2: "two",
    3: "three",
    4: "four",
    5: "five",
    6: "six",
    7: "seven",
    8: "eight",
    9: "nine",
    10: "ten",
    11: "eleven",
    12: "twelve",
    13: "thirteen",
    14: "fourteen",
    15: "fifteen",
    16: "sixteen",
    17: "seventeen",
    18: "eighteen",
    19: "nineteen",
    20: "twenty",
    30: "thirty",
    40: "forty",
    50: "fifty",
    60: "sixty",
    70: "seventy",
    80: "eighty",
    90: "ninety",
  };

  // E.x., 81, but not 80
  var hyphenNumber = function(input) {
    var factor = 10;
    var x = Math.floor(input/factor)*factor;
    var part1 = words[x];
    var part2 = words[Math.floor(input - x)];
    return [part1, part2].join('-');
  };

  var toNumeral = function(num) {
    if (words[num]) {
      return words[num];
    }

    if (num < 100) {
      return hyphenNumber(num);
    }

    var units = {
      100: "hundred",
      1000: "thousand,",
      1000000: "million,"
    };

    var steps = [
      [1000, 100],
      [1000000, 1000],
      [1000000000, 1000000]
    ];
    for (var i = 0; i < steps.length; i++) {
      var step = steps[i][0];
      var factor = steps[i][1];
      if (num < step) {
        var x = Math.floor(num/factor);
        var part1 = words[x];
        if (!part1) {
          part1 = numeralize(x);
        }
        var remainder = num - x * factor;
        var part2 = (remainder === 0) ? '' : numeralize(remainder);
        return [part1, units[factor], part2].join(' ').trim().replace(/,$/, '');
      }
    }
  };

  return toNumeral;
})();

root.numeralizeWithCent = function(input) {
  var part1 = numeralize(input);
  var part2 = input.toFixed(2).slice(-2) + '/100';
  return part1 + ' and ' + part2;
};

}.call(this));
